<?php 

#El index: en él mostraremos la salida de las vistas al usuario y también atravez de él enviaremos las distintas acciones que el usuario envie al controlador.

#require() establese que el código del archivo invocado es requerido, es decir, obligatorio para el funcionamiento del programa. por ello, si el archivo especificado en la función require() no se encuentra saltara un error "PHP fatal error" y el programa php se detendra.

#la versión require_once() funciona de la misma forma que su respectivo, salvo que, al utilizar la versión _once, se impide la carga de un mismo mas de una vez.

#si requerimos el mismo código más de una vez corremos el ríesgo de reclaraciones de variables, funciones o clases. 


require_once "controllers/controller.php";
require_once "models/model.php";

$mvc = new MvcController();
$mvc -> plantilla();

?>